const url = "https://lobinhos.herokuapp.com/wolves/";
let sectionGeral = document.querySelector(".sectionGeral")

/***
 *    ------------    ------    -----------  ----------   ------------ 
 *    ************   ********   ***********  ************ ************ 
 *    ---           ----------  ----    ---  --        -- ----         
 *    ***          ****    **** *********    **        ** ************ 
 *    ---          ------------ ---------    --        -- ------------ 
 *    ***          ************ ****  ****   **        **        ***** 
 *    ------------ ----    ---- ----   ----  ------------ ------------ 
 *    ************ ****    **** ****    **** **********   ************ 
 *                                                                     
 */

/*Cards*/

function createCards(element, side) {
    let sectionEscopo = document.createElement("section")
    sectionEscopo.classList.add("section-escopo")

    let sectionImg = document.createElement("section")
    let div = document.createElement("div")
    let img = document.createElement("img")
    sectionImg.classList.add("section-wolfs-img")
    img.src = `${element.link_image}`
    img.alt = "Imagem do lobo ao qual o texto se refere"
    sectionImg.append(div)
    sectionImg.append(img)

    let sectionTxt = document.createElement("section")
    let h2Name = document.createElement("h2")
    let h3Age = document.createElement("h3")
    let pDescription = document.createElement("p")

    h2Name.innerText = `${element.name}`
    h3Age.innerText = `Idade: ${element.age} anos`
    pDescription.innerText = `${element.description}`
    sectionTxt.append(h2Name)
    sectionTxt.append(h3Age)
    sectionTxt.append(pDescription)

    if (side == 0) {
        sectionEscopo.append(sectionImg)
        sectionEscopo.append(sectionTxt)
        sectionTxt.classList.add("section-wolfs-txt")
    } else {
        sectionEscopo.append(sectionTxt)
        sectionEscopo.append(sectionImg)
        sectionTxt.classList.add("section-wolfs-txtRight")
        div.classList.add("divRight")
    }

    sectionGeral.append(sectionEscopo)

}

/***
 *    -----------     ------    ------------ --------  ----    ----    ------    ------------    ------      --------   
 *    ************   ********   ************ ********  *****   ****   ********   ************   ********    **********  
 *    ---      ---  ----------  ----           ----    ------  ----  ----------  ---           ----------  ----    ---- 
 *    ************ ****    **** ****  ******   ****    ************ ****    **** ***          ****    **** ***      *** 
 *    -----------  ------------ ----  ------   ----    ------------ ------------ ---          ------------ ---      --- 
 *    ****         ************ ****    ****   ****    ****  ****** ************ ***          ************ ****    **** 
 *    ----         ----    ---- ------------ --------  ----   ----- ----    ---- ------------ ----    ----  ----------  
 *    ****         ****    **** ************ ********  ****    **** ****    **** ************ ****    ****   ********   
 *                                                                                                                      
 */

/*Paginação*/

function pagItens(resp, pageActual) {
    let resul = []
    let totalPage = Math.ceil(resp.length / 2)
    let count = (pageActual * 2) - 2
    let delimiter = count + 2;

    if (pageActual <= totalPage) {
        for (let i = count; i < delimiter; i++) {
            resul.push(resp[i])
            count++
        }
    }

    let side = 0
    resul.forEach(element => {
        if (side == 0) {
            createCards(element, side)
            side = 1
        } else {
            createCards(element, side)
            side = 0
        }
    })

    return;
}

fetch(url)
    .then(resp => resp.json()
        .then(resp => {
            pagItens(resp, 1)
        })
    )
    .catch(err => console.log(err))

let sectionSlide = document.querySelector(".section-adot")

/***
 *    ------------ ----         --------  ----------   ------------ 
 *    ************ ****         ********  ************ ************ 
 *    ----         ----           ----    --        -- ----         
 *    ************ ****           ****    **        ** ************ 
 *    ------------ ----           ----    --        -- ------------ 
 *           ***** ************   ****    **        ** ****         
 *    ------------ ------------ --------  ------------ ------------ 
 *    ************ ************ ********  **********   ************ 
 *                                                                  
 */

/*Slide*/

function slide(indice) {
    if (indice == 1) {
        sectionSlide.style.backgroundImage = "url('assets/wolf1.png')";
        setTimeout("slide(2)", 4000)
    } else if (indice == 2) {
        sectionSlide.style.backgroundImage = "url('assets/wolf2.png')";
        setTimeout("slide(3)", 4000)
    } else if (indice == 3) {
        sectionSlide.style.backgroundImage = "url('assets/wolf3.png')";
        setTimeout("slide(1)", 4000)
    }
}

slide(1)