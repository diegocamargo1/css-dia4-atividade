const url = "https://lobinhos.herokuapp.com/wolves/";
let sectionGeral = document.querySelector(".sectionGeral")

/***
 *    ----------   ------------ ----              ---      ---   --------   ----         ------------ 
 *    ************ ************ ****              ***  **  ***  **********  ****         ************ 
 *    --        -- ----         ----              ---  --  --- ----    ---- ----         ----         
 *    **        ** ************ ****              ***  **  *** ***      *** ****         ************ 
 *    --        -- ------------ ----              ---  --  --- ---      --- ----         ------------ 
 *    **        ** ****         ************      ************ ****    **** ************ ****         
 *    ------------ ------------ ------------       ----------   ----------  ------------ ----         
 *    **********   ************ ************        ********     ********   ************ ****         
 *                                                                                                    
 */

/*Deletar lobo*/

function excluir() {
    document.querySelector("div").style.display = "block"
}

let flag = 0

function deleteWolf(bool) {
    if (bool) {
        let fetchConfig = {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        }

        fetch(url + parametroUrl, fetchConfig)
            .then(resp => {
                document.querySelector(".pMensage").innerText = "Lobinho deletado com sucesso"
                document.querySelector(".buttonYes").style.display = "none"
                document.querySelector(".buttonNo").style.display = "none"
                document.querySelector(".buttonContinue").style.display = "inline"
                flag = 1
            })
    } else {
        document.querySelector(".buttonYes").style.display = "inline"
        document.querySelector(".buttonNo").style.display = "inline"
        document.querySelector("div").style.display = "none"
        if (flag == 1) {
            document.querySelector(".buttonContinue").style.display = "none"
            flag = 0
            window.location.href = "listaDeLobinhos.html?1"
        }
    }
}

/***
 *       ------    ----------     --------   ------------      ---      ---   --------   ----         ------------ 
 *      ********   ************  **********  ************      ***  **  ***  **********  ****         ************ 
 *     ----------  --        -- ----    ---- ------------      ---  --  --- ----    ---- ----         ----         
 *    ****    **** **        ** ***      ***     ****          ***  **  *** ***      *** ****         ************ 
 *    ------------ --        -- ---      ---     ----          ---  --  --- ---      --- ----         ------------ 
 *    ************ **        ** ****    ****     ****          ************ ****    **** ************ ****         
 *    ----    ---- ------------  ----------      ----           ----------   ----------  ------------ ----         
 *    ****    **** **********     ********       ****            ********     ********   ************ ****         
 *                                                                                                                 
 */

/*Adotar lobo*/

function adotar() {
    window.location.href = `adotar.html?${parametroUrl}`
}

/***
 *    ------------    ------    -----------  ---------- 
 *    ************   ********   ***********  ************
 *    ---           ----------  ----    ---  --        --         
 *    ***          ****    **** *********    **        **
 *    ---          ------------ ---------    --        -- 
 *    ***          ************ ****  ****   **        ** 
 *    ------------ ----    ---- ----   ----  ------------ 
 *    ************ ****    **** ****    **** ********** 
 *                                                                     
 */

/*Card*/

function createCards(element) {
    let sectionEscopo = document.createElement("section")
    sectionEscopo.classList.add("section-escopo")

    let h1Name = document.createElement("h1")
    let sectionData = document.createElement("section")
    let sectionImg = document.createElement("section")
    let div = document.createElement("div")
    let img = document.createElement("img")
    let sectionButton = document.createElement("section")
    let buttonAdotar = document.createElement("button")
    let buttonExcluir = document.createElement("button")

    let pDescription = document.createElement("p")

    h1Name.innerText = `${element.name}`
    img.src = `${element.link_image}`
    img.alt = "Imagem do lobo ao qual o texto se refere"
    pDescription.innerText = `${element.description}`
    buttonAdotar.innerText = "Adotar"
    buttonExcluir.innerText = "Excluir"

    sectionData.classList.add("sectionData")
    sectionImg.classList.add("sectionImg")
    sectionButton.classList.add("sectionButton")

    buttonAdotar.addEventListener("click", e => {
        e.preventDefault()
        adotar(element)
    })

    buttonExcluir.addEventListener("click", e => {
        e.preventDefault()
        excluir(element)
    })


    sectionButton.append(buttonAdotar)
    sectionButton.append(buttonExcluir)
    sectionImg.append(div)
    sectionImg.append(img)
    sectionImg.append(sectionButton)
    sectionData.append(sectionImg)
    sectionData.append(pDescription)
    sectionEscopo.append(h1Name)
    sectionEscopo.append(sectionData)

    sectionGeral.append(sectionEscopo)

}

let parametroUrl = window.location.href.split("?")[1];
console.log(url + parametroUrl)

fetch(url + parametroUrl)
    .then(resp => resp.json()
        .then(resp => {
            createCards(resp)
        })
    )
    .catch(err => console.log(err))

/***
 *    -----------    --------   ------------   --------   ------------ ------------ 
 *    ***********   **********  ************  **********  ************ ************ 
 *    ----       - ----    ---- ------------ ----    ---- ----         ----         
 *    ***********  ***      ***     ****     ***      *** ************ ************ 
 *    -----------  ---      ---     ----     ---      --- ------------ ------------ 
 *    ****       * ****    ****     ****     ****    **** ****                ***** 
 *    -----------   ----------      ----      ----------  ------------ ------------ 
 *    ***********    ********       ****       ********   ************ ************ 
 *                                                                                  
 */

/*Event Listener dos botões*/

let buttonYes = document.querySelector(".buttonYes")
let buttonNo = document.querySelector(".buttonNo")
let buttonContinue = document.querySelector(".buttonContinue")

buttonYes.addEventListener("click", e => {
    e.preventDefault()
    deleteWolf(true)
})
buttonNo.addEventListener("click", e => {
    e.preventDefault()
    deleteWolf(false)
})
buttonContinue.addEventListener("click", e => {
    e.preventDefault()
    deleteWolf(false)
})